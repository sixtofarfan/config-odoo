# WSGI Handler sample configuration file.
#
# Change the appropriate settings below, in order to provide the parameters
# that would normally be passed in the command-line.
# (at least conf['addons_path'])
#
# For generic wsgi handlers a global application is defined.
# For uwsgi this should work:
# $ uwsgi_python --http :9090 --pythonpath . --wsgi-file openerp-wsgi.py
#
# For gunicorn additional globals need to be defined in the Gunicorn section.
# Then the following command should run:
# $ gunicorn openerp:service.wsgi_server.application -c openerp-wsgi.py
import openerp
import multiprocessing
#----------------------------------------------------------
# Common
#----------------------------------------------------------
openerp.multi_process = True # Nah!
# Equivalent of --load command-line option
openerp.conf.server_wide_modules = ['web']
conf = openerp.tools.config
# Path to the OpenERP Addons repository (comma-separated for
# multiple locations)
conf['addons_path'] = '''/opt/odoo/odoo_git/odoo/addons,'''
# Optional database config if not using local socket
#conf['db_name'] = 'mycompany'
conf['db_host'] = '127.0.0.1'
conf['db_user'] = 'odoo'
conf['db_port'] = 5432
conf['db_password'] = 'DBPASSWORD'
conf['admin_passwd'] = 'ADMINPASSWORD'
conf['list_db'] = True
#conf['dbfilter'] = '^%h*\S*\-*$'
conf['dbfilter'] = u'^%d$'
#conf['log_handler'] = False
#conf['debug_mode'] = False
#AQUI
#Logging
#conf['logfile']='/tmp/openerp-server.log'
#conf['log_level']='debug'
#conf['syslog']='True'
#----------------------------------------------------------
# Generic WSGI handlers application
#----------------------------------------------------------
application = openerp.service.wsgi_server.application
openerp.service.server.load_server_wide_modules()
#----------------------------------------------------------
# Gunicorn
#----------------------------------------------------------
# Standard OpenERP XML-RPC port is 8069
bind = '127.0.0.1:8069'
pidfile = '.gunicorn.pid'
#workers = 1
workers = multiprocessing.cpu_count() * 2 + 1
#workers = multiprocessing.cpu_count()
timeout = 2000
max_requests = 2000


